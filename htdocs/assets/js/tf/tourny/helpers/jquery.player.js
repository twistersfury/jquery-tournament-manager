;(function($, window, document, undefined) {
	$.tf               = $.tf || {};
	$.tf.tourny        = $.tf.tourny || {};
	$.tf.tourny.helper = $.tf.tourny.helper || {};
	
	$.tf.tourny.helper.player = function(oTourny, oConfig) {
		this._oTourny = oTourny;
		this._oConfig = $.extend(
			true,
			{},
			$.tf.tourny.helper.player.prototype.defaultConfig,
			oConfig
		);
	};
	
	$.extend(
		true,
		$.tf.tourny.helper.player.prototype,
		{
			defaultConfig : {
				name         : '',
				ident        : null,
				totalMatches : 0,
				matchWins    : 0,
				matchDraws   : 0,
				matchByes    : 0,
				matchLoses   : 0,
				totalGames   : 0,
				gameWins     : 0,
				gameLoses    : 0,
				gameDraws    : 0,
				gameByes     : 0,
				matches      : []
			},
			getMatchPoints : function(bIncludeByes) {
				if ('undefined' == (typeof bIncludeByes)) {
					bIncludeByes = true;
				}
				return (this.matchWins + ((bIncludeByes) ? this.matchByes : 0)) * this._oTourny._oConfig.points.match;
			},
			getMatchWinPercentage : function(bIncludeByes) {
				if ('undefined' == (typeof bIncludeByes)) {
					bIncludeByes = false;
				}
				
				var fltResult = this.getMatchPoints(bIncludeByes) / (this.matchWins - ((bIncludeByes) ? this.matchByes : 0));
				if (fltResult < 0.33) {
					fltResult = 0.33;
				}
				
				return fltResult;
			},
			getOpMatchWinPercentag : function() {
				var tThis  = this;
				var iCount = 0;
				var fTotal = 0;
				$.each(this._oConfig.matches, function(iRound, oMatch) {
					if (!oMatch.isBye()) {
						iCount++;
						fTotal += oMatch.getOpponent(tThis._oConfig.ident).getMatchWinPercentage();
					}
				});
				
				fTotal = fTotal / iCount;
				if (fTotal < 0.33) {
					fTotal = 0.33;
				}
				
				return fTotal;
			},
			getGamePoints : function(bIncludeByes) {
				if ('undefined' == (typeof bIncludeByes)) {
					bIncludeByes = true;
				}
				
				return (this.gameWins + ((bIncludeByes) ? this.gameByes : 0)) * this._oTourny._oConfig.points.game;
			},
			getGameWinPercentage : function(bIncludeByes) {
				if ('undefined' == (typeof bIncludeByes)) {
					bIncludeByes = false;
				}
				
				var fltResult = this.getGamePoints(bIncludeByes) / (this.gameWins - ((bIncludeByes) ? this.gameByes : 0));
				if (fltResult < 0.33) {
					fltResult = 0.33;
				}
				return fltResult;
			},
			getOpGameWinPercentage : function() {
				var tThis = this;
				var iCount = 0;
				var fTotal = 0;
				$.each(this._oConfig.matches, function(iRound, oMatch) {
					if (!oMatch.isBye()) {
						iCount++;
						fTotal += oMatch.getOpponent(tThis._oConfig.ident).getGameWinPercentage();
					}
				});
				
				fTotal = fTotal / iCount;
				if (fTotal < 0.33) {
					fTotal = 0.33;
				}
				
				return fTotal;
			}
		}
	);
})(jQuery, window, document);