;(function($, window, document, undefined) {
	$.tf = $.tf || {};
	$.tf.tourny = $.tf.tourny || {};
	$.tf.tourny.deckcheck = function(oElement, oConfig) {
		this._$oElement = $(oElement);
		this._oConfig   = $.extend(
			true,
			{},
			$.tf.tourny.deckcheck,
			oConfig
		);
		
		this._init();
	};
	
	$.tf.tourny.deckcheck.prototype.defaultConfig = {
		elements : {
			
		}
	};
	
	$.tf.tourny.deckcheck.prototype._init = function() {
		var tThis = this;
		
		tThis.reset();
		
		tThis._$oElement.append(
			tThis.createElement('button')
				.html('1')
				.on('click', function(){
					tThis.updateCount(1);
				})
		).append(
			tThis.createElement('button')
				.html('2')
				.on('click', function() {
					tThis.updateCount(2);
				})
		).append(
			tThis.createElement('button')
				.html('3')
				.on('click', function() {
					tThis.updateCount(3);
				})
		).append(
			tThis.createElement('button')
				.html('4')
				.on('click', function() {
					tThis.updateCount(4);
				})
		).append(
			tThis.createElement('button')
				.html('Reset')
				.on('click', function() {
					tThis.createElement('dialog')
						.html('Are you sure?')
						.dialog({
							modal: true,
							width: 200,
							height: 100,
							buttons : {
								"Yes" : function() {
									$(this).dialog('close');
									tThis.reset();
								},
								"No" : function() {
									$(this).dialog('close');
								}
							},
							close : function() {
								$(this).dialog('destroy').remove();
							}
						});
				})
		).append(
			tThis.createElement('div')
				.attr('id', 'totals')
				.html(0)
		);
	};
	
	$.tf.tourny.deckcheck.prototype.updateCount = function(iCount) {
		var tThis = this;
		tThis._iCount += iCount;
		tThis._$oElement.find('#totals').html(tThis._iCount);
	};
	
	$.tf.tourny.deckcheck.prototype.reset = function() {
		this._iCount = 0;
		this._$oElement.find('#totals').html(0);
	};
	
	$.tf.tourny.deckcheck.prototype.createElement = function(sElement) {
		return $('<' + sElement + '>' + '</' + sElement + '>');
	};
	
	$.fn.tfDeck = function(vOptions) {
		if ('string' == (typeof vOptions)) {
			//TODO
		} else {
			return this.each(function() {
				if (!$(this).data('deck')) {
					$(this).data('deck', new $.tf.tourny.deckcheck(this, vOptions));
				}
			});
		}
	};
})(jQuery, window, document);