;(function($, window, document, undefined) {
	$.tf        = $.tf || {};	
	$.tf.tourny = $.tf.tourny || {};
	
	/**
	 * @
	 * @param oElement
	 * @param oConfig
	 */
	$.tf.tourny.base = function(oElement, oConfig) {
		this.logMessage('Construct');
		
		this._oConfig  = $.extend(
			true,
			{},
			$.tf.tourny.base.prototype.defaultConfig,
			oConfig
		);
		
		this._oGui = new $.tf.tourny.ui(oElement, this._oConfig.ui, this);
		
		this._init();
	};
	
	$.extend(
		true,
		$.tf.tourny.base.prototype,
		/** @base $.tf.tourny.base */
		{
			defaultConfig : {
				
			},
			_aTournies : [],
			_init : function() {
				var tThis = this;
				
				tThis.logMessage('Init');
				
				return tThis;
			},
			logMessage : function(vMessage, sIdent) {
				if ('undefined' == (typeof sIdent)) {
					sIdent = '$.tf.tourny.base';
				}
				
				sIdent += ': ';
				
				if ('string' == (typeof vMessage)) {
					return $.tf.logMessage(sIdent + vMessage);
				} else {
					$.tf.logMessage(sIdent);
					return $.tf.logMessage(vMessage);
				}
			},
			getFormats : function() {
				return $.tf.utils.filters.applyFilter('tourny.getFormats', []);
			},
			getPlayerSelects : function() {
				return $.tf.utils.filters.applyFilter('tourny.getPlayerSelects', []);
			},
			getGui : function() {
				return this._oGui;
			},
			startTournament : function(sName, sTourny, sPlayers) {
				$.tf.logMessage('Start Tournament: ' + sName + ':' + sTourny + ':' + sPlayers);
				
				
				var oElem   = this.getGui().createElement('div');
				var oTourny = $.tf.utils.getNamespaceObject(sTourny);
				oTourny     = new oTourny(oElem, this, {playerSelect : sPlayers});

				oElem.data('$.tf.tourny.type', oTourny);
				
				this._aTournies.push(oTourny);
				
				this.getGui().addTab($.tf.utils.underscore($.tf.utils.camelCaps(sName).replace(' ', '')), sName, oElem);
				
				return this;
			}
		}
	);
	
	$.fn.tfTourny = function(vOptions) {
		return this.each(function() {
			if (!$(this).data('tfTourny')) {
				$(this).data('tfTourny', new $.tf.tourny.base(this, vOptions));
			}
		});
	};
})(jQuery, window, document);