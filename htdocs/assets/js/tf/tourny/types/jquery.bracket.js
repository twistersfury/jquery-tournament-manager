;(function($, window, document, undefined) {

	$.tf.tourny.bracket = function(oElement, oConfig) {
		this._oElement = oElement;
		this._oConfig  = $.extend(
			true,
			{},
			$.tf.tourny.bracket.prototype.defaultConfig,
			oConfig
		);
		
		this._init();
	};
	
	$.extend(
		true,
		$.tf.tourny.bracket.prototype,
		{
			defaultConfig : {
				aPlayers : [],
				elements : {
					wrapper : {
						sElement : 'div',
						sClass   : 'jQBracket'
					}
				}
			},
			_init : function() {
				this._$eWrapper = this.createElement(this._oConfig.elements.wrapper.sElement).addClass(this._oConfig.elements.wrapper.sClass).appendTo(this._oElement);
				
				return this;
			},
			createElement : function(sElem) {
				return $('<' + sElem + '>' + '<' + '/' + sElem + '>');
			},
			renderer : {
				edit : function($eContainer, varValue, cbDone) {
					var $eInput = this.createElement('input').attr('type', 'text').val(varValue).blur(function() {
						cbDone($(this).val());
					}).keypress(function(oEvent) {
						var iKey = oEvent.KeyCode || oEvent.which;
						if (iKey === 13  /* Enter */ || iKey == 9 /* Tab */ || iKey === 27 /* Esc */) {
							oEvent.preventDefault();
							cbDone($(this).val());
						}
					}).focus();
					
					$eContainer.html($eInput);
					
					return this;
				},
				render : function($eContainer, $eTeam, $varScore) {
					$eContainer.append($eTeam);
					
					return this;
				}
			}
		}
	);
	
})(jQuery, window, document);