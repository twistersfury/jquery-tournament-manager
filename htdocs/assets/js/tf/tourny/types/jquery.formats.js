;(function($, window, document, undefined) {
	$.tf.tourny.formats = function(oElement, oTourny, oConfig) {
		this._oTourny  = oTourny;
		
		return $.tf.tourny.formats.parent.constructor.call(this, oElement, oConfig);
	};
	
	$.tf.tourny.formats.prototype   = $.extend(true, {}, $.tf.ui.base.prototype);
	//$.tf.tourny.ui.constructor = $.tf.ui.base;
	$.tf.tourny.formats.parent      = $.tf.ui.base.prototype;
	
	$.extend(
		true,
		$.tf.tourny.formats.prototype,
		{
			defaultConfig : {
				playerSelect : ''
			},
			_oElement : null,
			_oTourny  : null,
			_oConfig  : null,
			_oPlayerSelect : null,
			_aPlayers : [],
			_init : function() {
				var tThis = this;
				
				tThis.getElement()
					.append(
						tThis.createElement('button')
							.addClass('player-select')
							.on('click', function() {
								tThis.showPlayerSelect();
							})
					);
				
				return this;
			},
			showPlayerSelect : function() {
				var tThis = this;
				
				tThis.logMessage('showPlayerSelect');
				
				if (tThis._oPlayerSelect != null) {
					return tThis;
				}
				
				var tSelect = $.tf.utils.getNamespaceObject(tThis._oConfig.playerSelect);
				tSelect = new tSelect(tThis._aPlayers);
				
				tThis.createElement('div')
					.dialog({
						closeOnEscape : false,
						modal: false,
						open : function(oEvent, oUI) {
							$(oEvent.target).parent().draggable("option","containment", tThis._oTourny.getGui().getElement()).appendTo(tThis._oTourny.getGui().getElement());
							$(this).dialog("option", "position", "center");
						},
						buttons : {
							"Cancel" : function() {
								$(this).dialog("close");
							},
							"Done" : function() {
								tThis.addPlayers($(this).data('$.tf.tourny.select').getPlayers());
								$(this).dialog("close");
							}
						},
						close : function() {
							$(this).dialog("destroy").remove();
						}
				}).data('$.tf.tourny.select', tSelect);
					
			}
		}
	);
	
})(jQuery, window, document);