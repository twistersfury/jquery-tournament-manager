;(function($, window, document, undefined) {
	$.tf.tourny.formats = $.tf.tourny.formats || {};
	
	$.tf.tourny.formats.swiss = function(oElement, oTourny, oConfig) {
		$.tf.logMessage($.tf.tourny.formats.swiss.parent.constructor);
		$.tf.tourny.formats.swiss.parent.constructor.call(this, oElement, oTourny, oConfig);
	};
	
	//Defining Parent
	$.tf.tourny.formats.swiss.prototype            = Object.create($.tf.tourny.formats.prototype);
	$.tf.tourny.formats.swiss.parent               = $.tf.tourny.formats.prototype;
	$.tf.tourny.formats.swiss.prototype.constructor = $.tf.tourny.formats.swiss;
	
	//Default Configuration
	$.tf.tourny.formats.swiss.prototype.defaultConfig = {
		playerSelect : ''	
	};
	
	//Adding Filter
	$.tf.utils.filters.addFilter(
		'tourny.getFormats',
		function(aFormats) {
			aFormats.push(
				{
					ident : 'jQuery.tf.tourny.formats.swiss',
					title : 'Swiss'
				}
			);
			
			return aFormats;
		}
	);
})(jQuery, window, document);