;(function($, document, window, undefined) {
	$.tf             = $.tf             || {};
	$.tf.tourny      = $.tf.tourny      || {};
	$.tf.tourny.tabs = $.tf.tourny.tabs || {};
	
	/** @prop _oTourny $.tf.tourny.base */
	$.tf.tourny.tabs.create = function(oElement, oConfig, oTourny) {
		this._oTourny  = oTourny;
		this.logMessage('Construct');
		
		return $.tf.tourny.ui.parent.constructor.call(this, oElement, oConfig);
	};
	
	$.tf.tourny.tabs.create.parent = $.tf.ui.base.prototype;
	
	$.tf.tourny.tabs.create.prototype   = $.extend(true, {}, $.tf.ui.base.prototype);
	//$.tf.tourny.tabs.create.constructor = $.tf.ui.base;
	$.tf.tourny.tabs.create.parent      = $.tf.ui.base.prototype;
		
	$.extend(
		true,
		$.tf.tourny.tabs.create.prototype,
		{
			/** @param _oTourny $.tf.tourny.base */
			_oTourny : null,
			_init : function() {
				var tThis = this;
				
				$.tf.utils.filters.addFilter('create_on_close', function() {
					tThis._oTourny.getGui().showError('You cannot close the Create Tab!');
					return true;
				});
				
				tThis._buildName();
				tThis._buildTypeSelect();
				tThis._buildPlayerSelect();
				
				tThis.addPanel('Name'         , tThis._oName   , 'Enter any simple name (15 Characters or less)');
				tThis.addPanel('Format'       , tThis._oType   , 'Swiss, Single Elim, Double Elim, etc');
				tThis.addPanel('Player Select', tThis._oPlayers, 'Single, Team, Pods, etc');
				
				tThis.addElement(
					tThis.createElement('button')
						.addClass('start')
						.html('Start Tournament')
						.on('click', function() {
							tThis.startTournament();
						})
				);
			},
			_buildName : function() {
				var tThis = this;
				
				tThis._oName = tThis.createElement('input')
					.attr('id', 'create.name')
					.attr('type', 'text')
					.on('change', function() {
						if ($(this).val().length > 15) {
							tThis.showError("Name pust be 15 characters or less.");
						}
					});
				
				return tThis;
			},
			_buildTypeSelect : function() {
				var tThis = this;
				
				tThis._oType = tThis.createElement('select')
					.attr('id', 'create.type')
					.append(
						tThis.createElement('option')
							.html('-- SELECT ONE --')
							.val(0)
					);
				
				var aTournies = tThis._oTourny.getFormats();
				for(var iLoop in aTournies) {
					tThis._oType.append(
						tThis.createElement('option')
							.html(aTournies[iLoop].title)
							.val(aTournies[iLoop].ident)
					);
				}
				
				return tThis;
			},
			_buildPlayerSelect : function() {
				var tThis = this;
				
				tThis._oPlayers = tThis.createElement('select')
					.attr('id', 'create.players')
					.append(
						tThis.createElement('option')
							.html('-- SELECT ONE --')
							.val(0)
					);
				
				var aPlayers = tThis._oTourny.getPlayerSelects();
				for (var iLoop in aPlayers) {
					tThis._oPlayers.append(
						tThis.createElement('option')
							.html(aPlayers[iLoop].title)
							.val(aPlayers[iLoop].ident)
					);
				}
				
				return tThis;
			},
			startTournament : function() {
				var tThis = this;
				var bGood = true;
				var sName = tThis._oName.val();
				
				if (sName > 15) {
					tThis.showError("Name must be 15 characters or less.");
					bGood = false;
				} else if (sName.length == 0) {
					tThis.showError("Name must be set.");
					bGood = false;
				}
				
				if (tThis._oType.val() == 0) {
					tThis.showError("Please select a tournament type.");
					bGood = false;
				}
				
				if (tThis._oPlayers.val() == 0) {
					tThis._oPlayers.val('jQuery.tf.tourny.ui.player.selects.single');
				}
				
				if (!bGood) {
					return false;
				}
				
				if (tThis._oTourny.startTournament(sName, tThis._oType.val(), tThis._oPlayers.val())) {
					tThis._oName.val('');
					tThis._oType.val('');
					tThis._oPlayers.val('');
				}
				
				return true;
			},
			showError : function(sMessage) {
				this._oTourny.getGui().showError(sMessage);
				
				return this;
			}
		}
	);
})(jQuery, document, window);