;(function($, window, document, undefined) {
	$.tf        = $.tf || {};
	$.tf.tourny = $.tf.tourny || {};
	
	$.tf.tourny.ui = function(oElement, oConfig, oTourny) {
		this._oTourny  = oTourny;
		this.logMessage('Construct');
		
		return $.tf.tourny.ui.parent.constructor.call(this, oElement, oConfig);
	};
	
	$.tf.tourny.ui.parent = $.tf.ui.base.prototype;
	
	$.tf.tourny.ui.prototype   = $.extend(true, {}, $.tf.ui.base.prototype);
	//$.tf.tourny.ui.constructor = $.tf.ui.base;
	$.tf.tourny.ui.parent      = $.tf.ui.base.prototype;
		
	$.extend(
		true,
		$.tf.tourny.ui.prototype,
		{
			defaultConfig : {
				elements : {
					tabs : '.tabs'
				}
			},
			_init : function() {
				var tThis = this;
				
				tThis.logMessage('_init');
				
				//Adding Navigation
				tThis.getElement().append(tThis.createElement('ul'));
				
				tThis._buildCreateTab();
				
				tThis._initTabs();
			},
			addTab : function(sIdent, sName, oPanel, bClose) {
				var tThis = this;
				if ('undefined' == (typeof bClose)) {
					bClose = true;
				}
				
				if (tThis.getElement().hasClass('ui-tabs')) {
					tThis.getElement().tabs('destroy');
				}
				
				var tLi = tThis.createElement('li')
					.addClass(sIdent)
					.append(
						tThis.createElement('a')
						.attr('href', '#' + sIdent)
						.html(sName)
					);
				
				if (bClose) {
					tLi.append(
						tThis.createElement('span')
						.addClass('ui-tabs-closeable')
						.addClass('ui-icon ui-icon-circle-close')
						.html('&nbsp;')
						.on('click', function() {
							if (!($.tf.utils.filters.applyFilter(sIdent + '_on_close', false))) {
								tThis.removeTab(sIdent);
							}
						})
					);
				}
				
				tThis.getElement().children('ul').append(
					tLi	
				);
				
				tThis.getElement().append(
					tThis.createElement('div')
						.addClass(sIdent)
						.attr('id', sIdent)
						.append(oPanel)
				);
				
				tThis._initTabs();
				
				return tThis;
			},
			removeTab : function(sIdent) {
				var tThis = this;
				
				tThis.getElement().tabs('destroy');
				
				tThis.getElement().children('ul').find('.' + sIdent).remove();
				tThis.getElement().children('#' + sIdent).remove();
				
				tThis._initTabs();
				
				return tThis;
			},
			_initTabs : function() {
				this.getElement().tabs();
				
				return this;
			},
			_buildCreateTab : function() {
				var tThis = this;
				
				var oElem = tThis.createElement('div');
				oElem.data('$.tf.tourny.tabs.create', new $.tf.tourny.tabs.create(oElem, {}, this._oTourny));

				tThis.addTab('create', 'Create Tournament', oElem, false);
				
				return this;
			},
			logMessage : function(sMessage) {
				return $.tf.tourny.ui.parent.logMessage(sMessage, '$.tf.tourny.ui');
			},
			showError : function(sMessage, sType) {
				var tThis = this;
				
				sType = sType || 'Error';
				
				tThis.createElement('div')
					.attr('id', 'divError')
					.addClass('dialog-error')
					.html(sMessage)
					.dialog({
						modal: true,
						buttons : {
							'Okay' : function() {
								$(this).dialog('close');
							}
						},
						close : function() {
							$(this).dialog('destroy').remove();
						}
					});
			}
		}
	);
})(jQuery, window, document);