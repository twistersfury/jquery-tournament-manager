;(function($, window, document, undefined) {
	$.tf.tourny.ui.player = $.tf.tourny.ui.player || {};
	
	$.tf.tourny.ui.player.selects = function(oElement, oTourny, oConfig) {
		this._oTourny = oTourny;
		return $.tf.tourny.formats.parent.constructor.call(this, oElement, oConfig);
	};
	
	$.tf.tourny.ui.player.selects.prototype   = $.extend(true, {}, $.tf.ui.base.prototype);
	//$.tf.tourny.ui.constructor = $.tf.ui.base;
	$.tf.tourny.ui.player.selects.parent      = $.tf.ui.base.prototype;
	
	$.extend(
		true,
		$.tf.tourny.ui.player.selects.prototype,
		{
			defaultConfig : {
				
			},
			_init : function() {
				return this;
			},
			_baseInit : function() {
				return this;
			}
		}
	);
})(jQuery, window, document);