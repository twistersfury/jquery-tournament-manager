;(function($, window, document, undefined) {
	
	$.tf.tourny.ui.player = $.tf.tourny.ui.player || {};
	$.tf.tourny.ui.player.selects = $.tf.tourny.ui.player.selects || {};
	
	$.tf.tourny.ui.player.selects.single = function(oElement, oTourny, oConfig) {
		$.tf.tourny.ui.player.selects.single.parent.constructor.call(this, oElement, oConfig);
	};
	
	$.tf.tourny.ui.player.selects.single.prototype = $.extend(true, {}, $.tf.tourny.ui.player.selects.prototype);
	$.tf.tourny.ui.player.selects.single.parent    = $.tf.tourny.ui.player.selects.prototype;
		
	$.extend(
		true,
		$.tf.tourny.ui.player.selects.single.prototype,
		{
			defaultConfig : {

			}
		}
	);
	
	$.tf.utils.filters.addFilter(
		'tourny.getPlayerSelects',
		function(aFormats) {
			aFormats.push(
				{
					ident : 'jQuery.tf.tourny.ui.player.selects.single',
					title : 'Single'
				}
			);
			
			return aFormats;
		}
	);
})(jQuery, window, document);