;(function($, window, document, undefined) {
	$.tf.ui = $.tf.ui || {};
	
	$.tf.ui.base = function(oElement, oConfig) {
		this.logMessage('Contstruct');
		
		this._$oElement = $(oElement);
		this._oConfig  = $.extend(
			true,
			{},
			$.tf.tourny.ui.prototype.defaultConfig,
			oConfig
		);
		
		this._init();
	};
	
	$.extend(
		true,
		$.tf.ui.base.prototype,
		{
			defaultConfig : {
				elements : {}
			},
			_init : function() {
				this.logMessage('base:_init');
			},
			createElement : function(sElement) {
				this.logMessage('createElement: ' + sElement);
				
				return $('<' + sElement + '>' + '<' + '/' + sElement + '>');
			},
			getElement : function(sFilter, bExact, oElement) {
				this.logMessage('getElement: ' + sFilter);
				if ('undefined' == (typeof sFilter)) {
					return this._$oElement;
				}
				
				bExact = bExact || false;
				
				if (!bExact) {
					if ('undefined' == (typeof this._oConfig.elements[sFilter])) {
						throw new "Element Not Defined: " + sFilter;
					}
					sFilter = this._oConfig.elements[sFilter];
				}
				
				if ('undefined' == (typeof oElement)) {
					oElement = this._$oElement;
				}
				
				return oElement.find(sFilter);
			},
			addPanel : function(sTitle, oElement, sDesc) {
				var tThis = this;
				
				sDesc = sDesc || "";
				
				tThis.addElement(
					tThis.createElement('div')
						.addClass('panel')
						.append(
								tThis.createElement('label')
									.addClass('title')
									.html(sTitle)
						)
						.append(
								tThis.createElement('span')
									.addClass('desc')
									.html(sDesc)
						)
						.append(
								tThis.createElement('div')
									.addClass('content')
									.append(oElement)
						)
				);
				
				return tThis;
			},
			addElement : function(oElement) {
				this.getElement().append(oElement);
				return this;
			},
			logMessage : function(vMessage, sIdent) {
				sIdent = sIdent || '$.tf.ui.base';
				sIdent += ': ';
				
				if ('string' == (typeof vMessage)) {
					return $.tf.logMessage(sIdent + vMessage);
				} else {
					$.tf.logMessage(sIdent);
					return $.tf.logMessage(vMessage);
				}
			}
		}
	);
})(jQuery, window, document);