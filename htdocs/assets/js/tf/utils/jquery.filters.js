;(function($, window, document, undefined) {
	$.tf               = $.tf               || {};
	$.tf.utils         = $.tf.utils         || {};
	$.tf.utils.filters = $.tf.utils.filters || {};
	
	$.extend(
		true, 
		$.tf.utils.filters,
		{
			_oFilters : {
				hooks      : {},
				priorities : []
			},
			addFilter : function(sHook, cbFunction, iPriority) {
				iPriority = iPriority || 10;
				$.tf.logMessage(iPriority);
				if ('undefined' == (typeof $.tf.utils.filters._oFilters.hooks[sHook])) {
					$.tf.utils.filters._oFilters.hooks[sHook] = {};
				};
				
				if ('undefined' == (typeof $.tf.utils.filters._oFilters.hooks[sHook][iPriority])) {
					$.tf.utils.filters._oFilters.hooks[sHook][iPriority] = [];
				}
				
				if ($.inArray(iPriority, $.tf.utils.filters._oFilters.priorities) == -1) {
					$.tf.utils.filters._oFilters.priorities.push(iPriority);
				}
				
				$.tf.utils.filters._oFilters.hooks[sHook][iPriority].push(cbFunction);
			},
			applyFilter : function(sHook) {
				if ('undefined' == (typeof $.tf.utils.filters._oFilters.hooks[sHook])) {
					if (arguments.length == 1) {
						return false;
						
					}
					return Array.prototype.slice.call(arguments, 1, 2)[0];
				}
				
				var oItems = $.tf.utils.filters._oFilters.hooks[sHook];
				
				$.tf.utils.filters._oFilters.priorities.sort();
				
				for(var iPriority in $.tf.utils.filters._oFilters.priorities) {
					iPriority = $.tf.utils.filters._oFilters.priorities[iPriority];
					if ('undefined' != (typeof oItems[iPriority])) {
						for(var iIndex in oItems[iPriority]) {
							Array.prototype.splice.call(arguments, 1, 2, oItems[iPriority][iIndex].apply(this, Array.prototype.slice.call(arguments, 1)));
						}
					}
				}
				
				return Array.prototype.slice.call(arguments, 1, 2)[0];
			}
		}
	);
})(jQuery, window, document, undefined);