;(function($, window, document) {
	$.tf = $.tf || {};
	$.tf = $.tf || {};
	$.tf.debugMode = $.tf.debugMode || false;
	$.tf.logMessage = $.tf.logMessage || function(varMessage) {
		if ($.tf.debugMode && window.console && window.console.log) {
			window.console.log(varMessage);
			return true;
		}
		
		return false;
	};
	
	$.tf.utils = $.tf.utils || {};
	
	$.extend(
		true,
		$.tf.utils,
		{
			isNumeric : function(vNumber) {
				return !isNaN(parseFloat(vNumber)) && isFinite(vNumber);
			},
			strcmp : function(sCompare, sComparision) {
				return ((sCompare == sComparision) ? 0 : ((sCompare > sComparision) ? 1 : -1));
			},
			camelCaps : function(sCheck) {
			    return sCheck.replace(/(?:^|\s)\w/g, function(sMatch) {
			        return sMatch.toUpperCase();
			    });
			},
			underscore : function(sCheck) {
				return sCheck.replace('/(.)([A-Z])/', '$1_$2').toLowerCase();
			},
			shuffle : function(aShuffle) {
				var iCurrent = aShuffle.length;
				var iRandom, vValue;
				
				while(0 != iCurrent) {
					iRandom = Math.floor(Math.random() * iCurrent--);
					
					vValue = aShuffle[iRandom];
					aShuffle[iRandom] = aShuffle[iCurrent];
					aShuffle[iCurrent] = vValue;
				}
				
				return aShuffle;
			},
			getNamespaceObject : function(sName, oContext) {
				oContext = oContext || window;
				var aNames = sName.split('.');
				
				$.tf.logMessage(oContext);
				
				for(var iLoop in aNames) {
					if ('undefined' == (typeof oContext[aNames[iLoop]])) {
						throw "Context Doesn't Exist: " + aNames[iLoop];
						return false;
					}
					
					oContext = oContext[aNames[iLoop]];
				}
				
				return oContext;
			}
		}
	);
})(jQuery, window, document);