<!DOCTYPE html>
<html>
	<head>
		<title>Tournament | Twister's Fury</title>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		
		<script src="assets/js/tf/tourny/jquery.deckcheck.js"></script> 

		 
		<link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css" />
		
		<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
		<!--<meta name="viewport" content="user-scalable=no, width=device-width" />-->
		<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		
		<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/cupertino/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="assets/css/style.css" type="text/css" />
		
		<style>
			button {
				width: 100%;
				height: 60px;
				font-size: 1.5em;
			}
			
			#totals {
				width: 100%;
				text-align: center;
				font-size: 1.5em;
			}
		</style>
	</head>
	<body>
		<div class="tourny"></div>
		<script type="text/javascript">
			;(function($, window, document, undefined) {
				$(function() {
					$.tf.debugMode = true;
					$(".tourny").tfDeck();
				});
			})(jQuery, window, document);
		</script>
		<?php echo '<!--' . date('H:i:s') . '-->'; ?>
	</body>
</html>