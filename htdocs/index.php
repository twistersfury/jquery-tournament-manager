<!DOCTYPE html>
<html>
	<head>
		<title>Tournament | Twister's Fury</title>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		
		<script src="assets/js/tf/utils/jquery.utils.js"></script> 
		<script src="assets/js/tf/utils/jquery.filters.js"></script>
		<script src="assets/js/tf/ui/jquery.base.js"></script>
		<script src="assets/js/tf/tourny/helpers/jquery.player.js"></script>
		<script src="assets/js/tf/tourny/ui/jquery.tournament.js"></script>
		<script src="assets/js/tf/tourny/ui/jquery.create.js"></script>
		<script src="assets/js/tf/tourny/jquery.tournament.js"></script>
		<script src="assets/js/tf/tourny/types/jquery.formats.js"></script>
		<script src="assets/js/tf/tourny/types/jquery.swiss.js"></script>
		<script src="assets/js/tf/tourny/ui/player-selects/jquery.selects.js"></script>
		<script src="assets/js/tf/tourny/ui/player-selects/jquery.single.js"></script>
		 
		<link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
	</head>
	<body>
		<div class="tourny"></div>
		<script type="text/javascript">
			;(function($, window, document, undefined) {
				$(function() {
					$.tf.debugMode = true;
					$(".tourny").tfTourny();
				});
			})(jQuery, window, document);
		</script>
		<?php echo '<!--' . date('H:i:s') . '-->'; ?>
	</body>
</html>