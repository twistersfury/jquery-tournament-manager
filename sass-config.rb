# Require any additional compass plugins here.
# Set this to the root of your project when deployed:
http_path = "htdocs/"
sass_dir = "sass"
css_dir = "htdocs/assets/css"
images_dir = "htdocs/assets/images"
javascripts_dir = "htdocs/assets/js"
http_stylesheets_dir = "assets/css"
http_javascripts_dir = 'assets/cs'
http_images_dir = 'assets/images'
# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true